package it.deck47.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import hdi.lotto.OperazioniXMLLotto;
import it.deck47.db.DBOperationsHDI;

public class T_MPX_Dao extends UtilityDao implements IDao  {
	
//	public T_DOCUMENTO_Dao createValueObject() {
//        return new T_DOCUMENTO_Dao();
//    }
	
	public T_MPX_Dao(String schema) {
		initSchema(schema);
	}
	
	public int update(Connection conn, String ID_DELIVERY,String MPXSETID,String UPLOADDATE,String NOTIFYDATE) throws Exception{
		String Operazione = "UPDATE "+SCHEMA+".T_MPX  ";
		PreparedStatement statement = null;
		int result =-10;
		try {
			if(ID_DELIVERY==null)throw new Exception("ID_DELIVERY nullo");
			//String sqlCurrTipo="";if(CURR_TIPO>0) {	sqlCurrTipo = " ID_TIPO = "+ CURR_TIPO+" AND" ;	}
			//String statoSQL ="";if(CURR_STATO!=null && !CURR_STATO.isEmpty())statoSQL =" ID_STATO = "+CURR_STATO+" AND ";
			     statement = getPrepareStatment("UPDATE "+SCHEMA+".T_MPX " + 
			     		" SET  " + 
			     		" UPLOADDATE =  " + toDate(UPLOADDATE)+ " , "+
			     		" NOTIFYDATE =  " + toDate(NOTIFYDATE)+ " , "+
			     		" MPXSETID = ?" + 
			     		" WHERE ID_DELIVERY = ?" 
			     		,conn);
			      
			     statement.setString (1, MPXSETID);
			     statement.setString (2, ID_DELIVERY);
			    result= executeUpdate(statement);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =-123;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		if(result<1)throw new Exception("Errore in "+Operazione+" CODE:"+result);
		return result;
	  }  
	
	
	public int updateMXPfromCustomerSetID(Connection conn, String CustomerSetID,String MPXSETID,String UPLOADDATE,String NOTIFYDATE,String CODRACCOMANDATA, String MAILDATE) throws Exception{
		String Operazione = "UPDATE "+SCHEMA+".T_MPX  ";
		PreparedStatement statement = null;
		int result =-10;
		try {
			if(CustomerSetID==null)throw new Exception("CustomerSetID nullo");
			//String sqlCurrTipo="";if(CURR_TIPO>0) {	sqlCurrTipo = " ID_TIPO = "+ CURR_TIPO+" AND" ;	}
			//String statoSQL ="";if(CURR_STATO!=null && !CURR_STATO.isEmpty())statoSQL =" ID_STATO = "+CURR_STATO+" AND ";
			     statement = getPrepareStatment("UPDATE "+SCHEMA+".T_MPX " + 
			     		" SET  " + 
			     		" UPLOADDATE =  ? , "+
			     		" NOTIFYDATE =  ? , "+
			     		" MPXSETID = ?" + 
			     		" CODRACCOMANDATA = ?" + 
			     		" MAILDATE = ?" + 
			     		" where ID_DELIVERY = ( " + 
			     		"	select  ID_DELIVERY from T_LOTTO join T_R_CANALE ON T_R_CANALE.ID = T_LOTTO.ID_LOTTO" + 
			     		"	join T_DELIVERY ON T_DELIVERY.ID = T_R_CANALE.ID_DELIVERY" + 
			     		"	where T_LOTTO.\"CustomerSetID\" = ? " + 
			     		"	AND T_DELIVERY.ID_TIPO = 3" + 
			     		"	AND T_DELIVERY.ID_STATO = 0" + 
			     		" ) "
			     		,conn);
			      
			     statement.setString (1, UPLOADDATE);
			     statement.setString (2, NOTIFYDATE);
			     statement.setString (3, MPXSETID);
			     statement.setString (4, CODRACCOMANDATA);
			     statement.setString (5, MAILDATE);
			     
			     statement.setString (6, CustomerSetID);
			    result= executeUpdate(statement);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =-123;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		if(result<1)throw new Exception("Errore in "+Operazione+" CODE:"+result);
		return result;
	  }  
	
	
	public int updateMXPfromCustomerEnvelopID(Connection conn, String CustomerEnvelopID,String MPXSETID,String UPLOADDATE,String NOTIFYDATE,String CODRACCOMANDATA, String MAILDATE) throws Exception{
		String Operazione = "UPDATE "+SCHEMA+".T_MPX  ";
		PreparedStatement statement = null;
		int result =-10;
		try {
			if(CustomerEnvelopID==null)throw new Exception("CustomerEnvelopID nullo");
			//String sqlCurrTipo="";if(CURR_TIPO>0) {	sqlCurrTipo = " ID_TIPO = "+ CURR_TIPO+" AND" ;	}
			//String statoSQL ="";if(CURR_STATO!=null && !CURR_STATO.isEmpty())statoSQL =" ID_STATO = "+CURR_STATO+" AND ";
			     statement = getPrepareStatment("UPDATE "+SCHEMA+".T_MPX " + 
			     		" SET  " + 
			     		" UPLOADDATE =  ? , "+
			     		" NOTIFYDATE =  ? , "+
			     		" MPXSETID = ?" + 
			     		" CODRACCOMANDATA = ?" + 
			     		" MAILDATE = ?" + 
			     		" where ID_DELIVERY = ( " + 
			     		"	select  ID_DELIVERY from T_DOCUMENTO  " + 
			     		"    join T_R_CANALE ON T_R_CANALE.ID = T_DOCUMENTO.ID_DOCUMENTO" + 
			     		"	join T_DELIVERY ON T_DELIVERY.ID = T_R_CANALE.ID_DELIVERY" + 
			     		"	where T_DOCUMENTO.\"CustomerEnvelopeID\" = ? "+ 
			     		"	AND T_DELIVERY.ID_TIPO = 3" + 
			     		"	AND T_DELIVERY.ID_STATO = 0" + 
			     		" ) "
			     		,conn);
			      
			     statement.setString (1, UPLOADDATE);
			     statement.setString (2, NOTIFYDATE);
			     statement.setString (3, MPXSETID);
			     statement.setString (4, CODRACCOMANDATA);
			     statement.setString (5, MAILDATE);
			     
			     statement.setString (6, CustomerEnvelopID);
			    result= executeUpdate(statement);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =-123;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		if(result<1)throw new Exception("Errore in "+Operazione+" CODE:"+result);
		return result;
	  }  
	
	
	
	
	public Long  insert(Connection conn, Long idDelivery ) throws SQLException{
		 
		String Operazione = "INSERT "+SCHEMA+".T_MPX ";
		PreparedStatement statement   = null;
		Long risultati = -1l;
		try {
			  statement   =  getPrepareStatment("INSERT into "+SCHEMA+".T_MPX " + 
					" (   ID_DELIVERY  )" + 
					"  VALUES  " + 
					" ( ? )",conn);
			  		statement.setLong(1, idDelivery);
				  risultati = executeInsert(statement,false);
				  
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	risultati =-2l;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return risultati;
	  }  
	
	
	
	
	
	

}
