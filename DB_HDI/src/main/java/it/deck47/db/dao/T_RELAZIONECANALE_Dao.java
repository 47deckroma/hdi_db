package it.deck47.db.dao;

import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.util.ArrayList;
import java.util.Arrays;

public class T_RELAZIONECANALE_Dao extends UtilityDao implements IDao  {
	
//	public T_RELAZIONECANALE_Dao createValueObject() {
//        return new T_RELAZIONECANALE_Dao();
//    }
	

	public T_RELAZIONECANALE_Dao(String schema) {
		initSchema(schema);
	}
	public Long insert(Connection conn,  Long id, Long idDelivery) throws SQLException{
		String Operazione = "INSERT "+SCHEMA+".T_R_CANALE ";
		PreparedStatement statement = null;
		Long result =null;
		try {
			     statement = getPrepareStatment("INSERT into "+SCHEMA+".T_R_CANALE " + 
			      " (  ID,ID_DELIVERY) " + 
			      "  VALUES  " + 
			      " (?,?)",conn);
			    // System.out.println(id+" - "+idDelivery);
			    statement.setLong(1, id);
			    statement.setLong(2,idDelivery);
			    result= executeInsert(statement,false);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =null;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return result;
	  }  
	
	public int insertList(Connection conn, ArrayList<Long> idDocument, ArrayList<Long> idDelivery) throws Exception{
		String Operazione = "INSERT "+SCHEMA+".T_R_CANALE ";
		PreparedStatement statement = null;
		int result=-1;
		if(idDocument.size()!= idDelivery.size())throw new Exception("Inaspettata differenza tra numero documenti e numero delivery");
		try {
				int index=0;
				statement = getPrepareStatment("INSERT into "+SCHEMA+".T_R_CANALE " + 
						" (  ID,ID_DELIVERY) " + 
						"  VALUES  " + 
						" (?,?)",conn);
				for (Long idDoc : idDocument) {
					// System.out.println(id+" - "+idDelivery);
					statement.setLong(1, idDoc);
					statement.setLong(2,idDelivery.get(index));
					index++;
					statement.addBatch(); 
				}
				int[] risultati = executeInsertBatch(statement);
				result = sumOfElements(risultati);
				log.info("Aggiunti DOC sul DB " + Arrays.toString(risultati)  +" Totale: "+result);
				if((index)!=result)throw new Exception("Relazioni aggiornati/inseriti difforme da quanto atteso");
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =-1;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return result;
	  }  
	
	

}
