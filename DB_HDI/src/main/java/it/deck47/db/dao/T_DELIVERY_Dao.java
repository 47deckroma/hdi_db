package it.deck47.db.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.JDBCType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;

public class T_DELIVERY_Dao extends UtilityDao implements IDao  {
	
	public T_DELIVERY_Dao(String schema) {
		initSchema(schema);
	}
//	public T_DELIVERY_Dao createValueObject() {
//        return new T_DELIVERY_Dao();
//    }
	public Long insert(Connection conn,  int tipo, int stato) throws SQLException{
		String Operazione = "INSERT "+SCHEMA+".T_DELIVERY ";
		PreparedStatement statement = null;
		Long result =null;
		try {
			     statement = getStatment("INSERT into "+SCHEMA+".T_DELIVERY " + 
			      " (  ID,ID_TIPO,ID_STATO,\"DataNotifica\",\"DataEsito\",IDENTIFICATIVO ) " + 
			      "  VALUES  " + 
			      " ("+SCHEMA+".T_DELIVERY_SEQ.nextval,?,?,SYSDATE,?,?)","ID",conn);
			    statement.setLong(1, tipo);
			    statement.setLong(2, stato);
			   // statement.setString(3, "SYSDATE" );
			    statement.setNull(3, JDBCType.TIMESTAMP.getVendorTypeNumber());
			    statement.setNull(4, JDBCType.VARCHAR.getVendorTypeNumber());
			    result= executeInsert(statement,true);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =null;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return result;
	  }  
	
	public ArrayList<Long> insert(Connection conn,  int tipo, int stato,int numerosita) throws SQLException{
		String Operazione = "INSERT "+SCHEMA+".T_DELIVERY ";
		PreparedStatement statement = null;
		ArrayList<Long> resultList=null;
		try {
			statement = getStatment("INSERT into "+SCHEMA+".T_DELIVERY " + 
					" (  ID,ID_TIPO,ID_STATO,\"DataNotifica\",\"DataEsito\",IDENTIFICATIVO ) " + 
					"  VALUES  " + 
					" ("+SCHEMA+".T_DELIVERY_SEQ.nextval,?,?,SYSDATE,?,?)","ID",conn);
			     for(int i=0;i<numerosita;i++) {
					    statement.setLong(1, tipo);
					    statement.setLong(2, stato);
					    //java.util.Date today = new java.util.Date();
					   //statement.setTimestamp(3, new java.sql.Timestamp(today.getTime()));
					    statement.setNull(3, JDBCType.TIMESTAMP.getVendorTypeNumber());
					    statement.setNull(4, JDBCType.VARCHAR.getVendorTypeNumber());
					    statement.addBatch();
			     }
			    int[] risultati = executeInsertBatch(statement);
			    ResultSet rs = statement.getGeneratedKeys();
				   resultList = new ArrayList<Long>();
			        while (rs.next()) {
			        	resultList.add(rs.getLong(1));
			        }
			    int documentiAggiornati = sumOfElements(risultati);
				log.info("Aggiunti STATI sul DB " + Arrays.toString(risultati)  +" Totale: "+documentiAggiornati);
				if(numerosita!=documentiAggiornati)throw new Exception("DELIVERY aggiornati/inseriti difforme da quanto atteso");

		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	resultList =null;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return resultList;
	  }
	
	
	

}
