package it.deck47.db.dao;

import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import hdi.lotto.OperazioniXMLLotto;
import it.deck47.db.DBOperationsHDI;

public class T_SMS_Dao extends UtilityDao implements IDao  {
	
//	public T_DOCUMENTO_Dao createValueObject() {
//        return new T_DOCUMENTO_Dao();
//    }
	
	public T_SMS_Dao(String schema) {
		initSchema(schema);
	}
	
	public Long insert(Connection conn, String CUSTOMERENVELOPID, String TESTO,String NUMERO,String ERRMESS,String ESITO,String DATAINVIO) throws Exception{
		String Operazione = "INSERT "+SCHEMA+".T_SMS  ";
		PreparedStatement statement = null;
		Long result =(long) -10;
		try {
			     statement = getPrepareStatment("INSERT INTO "+SCHEMA+".T_SMS " + 
			     		"(ID_DELIVERY,TESTO, NUMERO, ERRMESS, DATAINVIO, ESITO)" +
			     		"  VALUES  " + 
			     				"((select ID_DELIVERY from T_DOCUMENTO   " + 
					     		"			     join T_R_CANALE ON T_R_CANALE.ID = T_DOCUMENTO.ID_DOCUMENTO " + 
					     		"			     join T_DELIVERY ON T_DELIVERY.ID = T_R_CANALE.ID_DELIVERY " + 
					     		"			     where T_DOCUMENTO.\"CustomerEnvelopeID\" = ? " + 
					     		"			     AND T_DELIVERY.ID_TIPO = 3" + 
					     		"			     AND T_DELIVERY.ID_STATO = 0 ),?,?,?,?,?)",conn);
			     
				 Timestamp datetime = Timestamp.valueOf(DATAINVIO);
			     
				 statement.setString (1, CUSTOMERENVELOPID); 
			     statement.setString (2, TESTO);
			     statement.setString (3, NUMERO);
			     statement.setString (4, ERRMESS);
			     statement.setTimestamp(5, datetime);
			     statement.setString (6, ESITO);
			    result= (long) executeInsert(statement,false);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =(long) -123;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		if(result<1)throw new Exception("Errore in "+Operazione+" CODE:"+result);
		return result;
	  }  

}
