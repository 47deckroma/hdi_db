package it.deck47.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import hdi.lotto.OperazioniXMLLotto;
import it.deck47.db.DBOperationsHDI;

public class T_DOCUMENTO_Dao extends UtilityDao implements IDao  {
	
//	public T_DOCUMENTO_Dao createValueObject() {
//        return new T_DOCUMENTO_Dao();
//    }
	
	public T_DOCUMENTO_Dao(String schema) {
		initSchema(schema);
	}
	
	public int update(Connection conn, String CustomerSetID,int CURR_TIPO,String CURR_STATO,int NEXT_STATUS) throws SQLException{
		String Operazione = "UPDATE "+SCHEMA+".T_DOCUMENTO STATUS";
		PreparedStatement statement = null;
		int result =-10;
		try {
			if(CustomerSetID==null)throw new Exception("CustomerSetID nullo");
			String sqlCurrTipo="";if(CURR_TIPO>0) {	sqlCurrTipo = " ID_TIPO = "+ CURR_TIPO+" AND" ;	}
			String statoSQL ="";if(CURR_STATO!=null && !CURR_STATO.isEmpty())statoSQL =" ID_STATO = "+CURR_STATO+" AND ";
			     statement = getPrepareStatment("UPDATE "+SCHEMA+".T_DELIVERY " + 
			     		" set ID_STATO = "+NEXT_STATUS+" , \"DataEsito\" = SYSDATE " + 
			     		" where   "+sqlCurrTipo + statoSQL+
			     		" ID  IN ( " + 
			     		" select  "+SCHEMA+".T_R_CANALE.ID_DELIVERY from "+SCHEMA+".T_R_CANALE " + 
			     		" join  "+SCHEMA+".T_DOCUMENTO ON "+SCHEMA+".T_DOCUMENTO.ID_DOCUMENTO = "+SCHEMA+".T_R_CANALE.ID" + 
			     		" join "+SCHEMA+".T_LOTTO ON "+SCHEMA+".T_LOTTO.ID_LOTTO = "+SCHEMA+".T_DOCUMENTO.ID_LOTTO" + 
			     		" where  "+SCHEMA+".T_LOTTO.\"CustomerSetID\" = ?"+
			     		" GROUP BY "+SCHEMA+".T_R_CANALE.ID_DELIVERY )" 
			     		,conn);
			      
			     statement.setString (1, CustomerSetID);
			    result= executeUpdate(statement);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =-123;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return result;
	  }  
	
	
	public int updateByCustomerEnvelop(Connection conn, String CustomerEnvelopeID,int CURR_TIPO,String CURR_STATO,int NEXT_STATUS) throws SQLException{
		String Operazione = "UPDATE "+SCHEMA+".T_DOCUMENTO STATUS";
		PreparedStatement statement = null;
		int result =-10;
		try {
			if(CustomerEnvelopeID==null)throw new Exception("CustomerEnvelopeID nullo");
			String sqlCurrTipo="";if(CURR_TIPO>0) {	sqlCurrTipo = " ID_TIPO = "+ CURR_TIPO+" AND" ;	}
			String statoSQL ="";if(CURR_STATO!=null && !CURR_STATO.isEmpty())statoSQL =" ID_STATO = "+CURR_STATO+" AND ";
			     statement = getPrepareStatment("UPDATE "+SCHEMA+".T_DELIVERY " + 
			     		" set ID_STATO = "+NEXT_STATUS+" , \"DataEsito\" = SYSDATE " + 
			     		" where   "+sqlCurrTipo + statoSQL+
			     		" ID  IN ( " + 
			     		" select  "+SCHEMA+".T_R_CANALE.ID_DELIVERY from "+SCHEMA+".T_R_CANALE " + 
			     		" join  "+SCHEMA+".T_DOCUMENTO ON "+SCHEMA+".T_DOCUMENTO.ID_DOCUMENTO = "+SCHEMA+".T_R_CANALE.ID" + 
			     		" where  "+SCHEMA+".T_DOCUMENTO.\"CustomerEnvelopeID\" = ?"+
			     		" GROUP BY "+SCHEMA+".T_R_CANALE.ID_DELIVERY )" 
			     		,conn);
			      
			     statement.setString (1, CustomerEnvelopeID);
			    result= executeUpdate(statement);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =-123;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return result;
	  }  
	
	public List<Long>  insert(Connection conn, List<hdi.lotto.Lotto.Documento> valuesObject,Long idLotto,String TipoAmbiente) throws SQLException{
		String Operazione = "INSERT "+SCHEMA+".T_DOCUMENTO ";
		PreparedStatement statement   = null;
		List<Long> resultList = null;
		try {
			if(valuesObject==null)throw new Exception("Documenti nulli");
			String [] ids={"ID_DOCUMENTO"};
			  statement   =  getStatment("INSERT into "+SCHEMA+".T_DOCUMENTO " + 
					" (   ID_DOCUMENTO,ID_LOTTO,\"CustomerEnvelopeID\",\"EUserZCode\" )" + 
					"  VALUES  " + 
					" ("+SCHEMA+".T_DOCUMENTO_SEQ.nextval,?,?,?)",ids,conn);
				int countDocumenti=0;
			  for  (hdi.lotto.Lotto.Documento valueObject : valuesObject) {
					if(valueObject==null)throw new Exception("Documento nullo");
					   statement.setLong(1, idLotto);
					    //documento[*]/contratto/numero-polizza  //documento[*]/punto-vendita/@codice  //documento[*]/contratto/id-movimento  //documento[*]/contratto/causale/@codice  //documento[*]/testata/codice-documento  //documento[*]/testata/tipologia-dms  Valore configurato su file di properties
					   statement.setString(2, OperazioniXMLLotto.getCustomerEnvelopeID(valueObject,TipoAmbiente));
					   //documento[*]/testata/servizio/centro-di-costo
					     statement.setString(3, valueObject.getTestata().getServizio().getCentroDiCosto() );
					    statement.addBatch();
					    countDocumenti++;
				}
				int[] risultati = executeInsertBatch(statement);
				 ResultSet rs = statement.getGeneratedKeys();
				 resultList = new ArrayList<Long>();
			        while (rs.next()) {
			        	resultList.add(rs.getLong(1));
			        }
			    int documentiAggiornati = sumOfElements(risultati);
				log.info("Aggiunti DOC sul DB " + Arrays.toString(risultati)  +" Totale: "+countDocumenti);
				if(countDocumenti!=documentiAggiornati)throw new Exception("Documenti aggiornati/inseriti difforme da quanto atteso");
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	resultList =null;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return resultList;
	  }  
	
	
	public HashMap<String,Long>  insertMap(Connection conn, List<hdi.lotto.Lotto.Documento> valuesObject,Long idLotto,String TipoAmbiente) throws SQLException{
		String Operazione = "INSERT "+SCHEMA+".T_DOCUMENTO ";
		PreparedStatement statement   = null;
		HashMap<String,Long>  resultList = null;
		try {
			if(valuesObject==null)throw new Exception("Documenti nulli");
			String [] ids={"ID_DOCUMENTO" };
			  statement   =  getStatment("INSERT into "+SCHEMA+".T_DOCUMENTO " + 
					" (   ID_DOCUMENTO,ID_LOTTO,\"CustomerEnvelopeID\",\"EUserZCode\" )" + 
					"  VALUES  " + 
					" ("+SCHEMA+".T_DOCUMENTO_SEQ.nextval,?,?,?)",ids,conn);
				int countDocumenti=0;
			  for  (hdi.lotto.Lotto.Documento valueObject : valuesObject) {
					if(valueObject==null)throw new Exception("Documento nullo");
					   statement.setLong(1, idLotto);
					    //documento[*]/contratto/numero-polizza  //documento[*]/punto-vendita/@codice  //documento[*]/contratto/id-movimento  //documento[*]/contratto/causale/@codice  //documento[*]/testata/codice-documento  //documento[*]/testata/tipologia-dms  Valore configurato su file di properties
					   statement.setString(2, OperazioniXMLLotto.getCustomerEnvelopeID(valueObject,TipoAmbiente));
					   //documento[*]/testata/servizio/centro-di-costo
					     statement.setString(3, valueObject.getTestata().getServizio().getCentroDiCosto() );
					    statement.addBatch();
					    countDocumenti++;
				}
				int[] risultati = executeInsertBatch(statement);
				 ResultSet rs = statement.getGeneratedKeys();
			        ArrayList<Long> ArrayId = new ArrayList<Long>();
				 	while (rs.next()) {
				 		ArrayId.add(rs.getLong(1));
			        }
			        Statement statementQuery = getStatment(conn);   
			        //OracleConnection oraConn = conn.unwrap(OracleConnection.class);
					//Array array = oraConn.createARRAY("T_DOCUMENTO.ID_DOCUMENTO", longArr);
			        ArrayList<ArrayList<Long>> arrIds = DBOperationsHDI.getChunkListLong(ArrayId, 999);
			        rs = executeQuery(statementQuery,"SELECT * FROM "+SCHEMA+".T_DOCUMENTO " + 
							"  WHERE ID_DOCUMENTO IN ("+arrIds.get(0).toString().substring(1, arrIds.get(0).toString().length()-1)+")");
			        resultList = new HashMap<String,Long>();
			        while (rs.next()) {
			        	resultList.put(rs.getString("CustomerEnvelopeID"),rs.getLong("ID_DOCUMENTO"));
			        }
			        rs.close();
			    int documentiAggiornati = sumOfElements(risultati);
				log.info("Aggiunti DOC sul DB " + Arrays.toString(risultati)  +" Totale: "+countDocumenti);
				if(countDocumenti!=documentiAggiornati)throw new Exception("Documenti aggiornati/inseriti difforme da quanto atteso");
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	e.printStackTrace();
	    	resultList =null;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return resultList;
	  }  
	
	
	
//	public void getEmps() throws SQLException{
//	    PreparedStatement statement = conn.prepareStatement("select "versione-template",CustomerSetID,WorkProcessID,RagioneSocialeMittente from T_LOTTO where ID_LOTTO = :ID_LOTTO");
//	    statement.setObject(?, "VALUE FOR ID_LOTTO");
//	    ResultSet resultSet = statement.executeQuery();
//	    while(resultSet.next()){
//	       System.out.println(resultSet.getObject(ID_LOTTO));
//	       System.out.println(resultSet.getObject("versione-template"));
//	       System.out.println(resultSet.getObject(CustomerSetID));
//	       System.out.println(resultSet.getObject(WorkProcessID));
//	       System.out.println(resultSet.getObject(RagioneSocialeMittente));
//	    }
//	  }   
	 
	
	

}
