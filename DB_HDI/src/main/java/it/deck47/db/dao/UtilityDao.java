package it.deck47.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Logger;


public class UtilityDao {
	protected static final Logger log = Logger.getLogger(IDao.class.getName());
	
	protected String SCHEMA="HDI";
	public boolean initSchema(String schema) {
		SCHEMA = schema;
		return true;
	}  
	//public String SCHEMA="ADOBE_HUB_COM";
	
	protected PreparedStatement getStatment(String query,String ID,Connection conn) throws SQLException {
		String key[] = {ID.toString()}; //put the name of the primary key column
	    PreparedStatement statement = conn.prepareStatement(query, key);
	    return statement;
	}
	
	protected PreparedStatement getStatment(String query,String[] key,Connection conn) throws SQLException {
	    PreparedStatement statement = conn.prepareStatement(query, key);
	    return statement;
	}
	
	protected PreparedStatement getPrepareStatment(String query ,Connection conn) throws SQLException {
		//System.out.println(query);
		PreparedStatement statement = conn.prepareStatement(query);
	    return statement;
	}
	protected Statement getStatment( Connection conn) throws SQLException {
		Statement statement = conn.createStatement();// prepareStatement(query);
	    return statement;
	}
	
	
	
	protected ResultSet executeQuery(PreparedStatement statement  ) throws SQLException {
		ResultSet rs =null;
		try {
			  rs = statement.executeQuery();
		}catch(Exception e) {
			log.severe("[ERRORE][SQL]executeQuery : "+e.getMessage());
	    }finally {
	    	 
	    }
	    return rs;
	}
	
	protected ResultSet executeQuery( Statement statement,String SQL  ) throws SQLException {
		ResultSet rs =null;
		try {
			  rs = statement.executeQuery(SQL);
		}catch(Exception e) {
			log.severe("[ERRORE][SQL]executeQuery : "+e.getMessage());
	    }finally {
	    	 
	    }
	    return rs;
	}
	
	protected String toDate(String stringDate) {
		return " TO_DATE('"+stringDate+"', 'YYYY-MM-DD HH:MI:SS') ";
	}
	
	protected String getIN(ArrayList<String> stringData) {
		String sql = "(";	
		for (String currStr : stringData) {
			sql = sql +   currStr +",";
			}
		sql = sql.substring(0,sql.length()-1) + ")";
		return sql;
	}
 	
	protected Long executeInsert(PreparedStatement statement ,boolean generateKey) throws SQLException {
		Long generatedKey=null;
		 ResultSet rs =null;
		try {
			int resultUpdate = statement.executeUpdate();
			if(resultUpdate>0) {
				if(generateKey) {
				    rs = statement.getGeneratedKeys();
				    if (rs.next()) {
				        generatedKey = rs.getLong(1);
				    }
				}else {
					generatedKey=new Long(resultUpdate);
				}
			}
		}catch(Exception e) {
			log.severe("[ERRORE][SQL]executeUpdate : "+e.getMessage());
	    }finally {
	    	if(rs!=null) {
	    		rs.close();
	    	}
	    }
	    return generatedKey;
	}
	
	protected int executeUpdate(PreparedStatement statement  ) throws SQLException {
		int resultUpdate =-1;
		 ResultSet rs =null;
		try {
			resultUpdate = statement.executeUpdate();
		}catch(Exception e) {
			log.severe("[ERRORE][SQL]executeUpdate : "+e.getMessage());
	    }finally {
	    	if(rs!=null) {
	    		rs.close();
	    	}
	    }
	    return resultUpdate;
	}
	
	
	protected int[] executeInsertBatch(PreparedStatement statement ) throws SQLException {
		int[] risultati = null;
		try {
			  risultati = statement.executeBatch();
		}catch(Exception e) {
			e.printStackTrace();
			log.severe("[ERRORE][SQL]executeInsertBatch : "+e.getMessage());
	    }finally {
	    	 
	    }
	    return risultati;
	}
	
	public static int sumOfElements(int[] array) {
		  int sum = 0;
		  if(array!=null)
			  for (int i : array) {
				  if(i==-2)
					  sum = sum + 1;
				  else
					  sum = sum + i;
			  }
		  return sum;
		  }
	
	public static long sumOfElements(long[] array) {
		  long sum = 0;
		  if(array!=null)
			  for (long i : array) {
				  if(i==-2)
					  sum = sum + 1;
				  else
					  sum = sum + i;
			  }
		  return sum;
		  }

}
