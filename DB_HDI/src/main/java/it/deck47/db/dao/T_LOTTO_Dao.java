package it.deck47.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import hdi.lotto.OperazioniXMLLotto;

public class T_LOTTO_Dao extends UtilityDao implements IDao  {
	
	public T_LOTTO_Dao(String schema) {
		initSchema(schema);
	}
	
//	public T_LOTTO_Dao createValueObject() {
//        return new T_LOTTO_Dao();
//    }
	
	
	
	public Long insert(Connection conn, hdi.lotto.Lotto valueObject) throws SQLException{
		String Operazione = "INSERT "+SCHEMA+".T_LOTTO ";
		PreparedStatement statement = null;
		Long result =null;
		try {
			if(valueObject==null)throw new Exception("Lotto nullo");
			     statement = getStatment("INSERT into "+SCHEMA+".T_LOTTO " + 
			      " (  ID_LOTTO,\"versione-template\",\"CustomerSetID\",\"WorkProcessID\",\"RagioneSocialeMittente\")" + 
			      "  VALUES  " + 
			      " ("+SCHEMA+".T_LOTTO_SEQ.nextval,?,?,?,?)", "ID_LOTTO",conn);
			    statement.setString (1, valueObject.getVersioneTemplate());
			    statement.setString(2,OperazioniXMLLotto.getCustomerSetID(valueObject)  );
			    statement.setString(3, valueObject.getServizio().getTipoInvio() );
			    String ragSoc="";
			    if(valueObject.getServizio()!=null&& valueObject.getServizio().getMittente()!=null&&valueObject.getServizio().getMittente().getRagioneSociale()!=null)ragSoc = valueObject.getServizio().getMittente().getRagioneSociale();
			    statement.setObject(4, ragSoc);
			    result= executeInsert(statement,true);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =null;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return result;
	  }  
	
	public int update(Connection conn, String CustomerSetID,int CURR_TIPO,String CURR_STATO,int NEXT_STATUS) throws SQLException{
		String Operazione = "UPDATE "+SCHEMA+".T_LOTTO STATUS";
		PreparedStatement statement = null;
		int result =-10;
		try {
			if(CustomerSetID==null)throw new Exception("CustomerSetID nullo");
 			String sqlCurrTipo="";if(CURR_TIPO>0) {
				sqlCurrTipo = " ID_TIPO = "+ CURR_TIPO+" AND" ;
			}
			String statoSQL ="";if(CURR_STATO!=null && !CURR_STATO.isEmpty())statoSQL =" ID_STATO = "+CURR_STATO+" AND ";
			     statement = getPrepareStatment("UPDATE "+SCHEMA+".T_DELIVERY " + 
			     		" set ID_STATO = "+NEXT_STATUS+" , \"DataEsito\" = SYSDATE " + 
			     		" where   "+sqlCurrTipo + statoSQL+
			     		" ID  IN ( " + 
			     		" select  "+SCHEMA+".T_R_CANALE.ID_DELIVERY from "+SCHEMA+".T_R_CANALE " + 
			     		" join  "+SCHEMA+".T_LOTTO ON "+SCHEMA+".T_LOTTO.ID_LOTTO = "+SCHEMA+".T_R_CANALE.ID " + 
			     		" where  "+SCHEMA+".T_LOTTO.\"CustomerSetID\" = ? " + 
			     		" GROUP BY "+SCHEMA+".T_R_CANALE.ID_DELIVERY " + 
			     		")",conn);
			      
			     statement.setString (1, CustomerSetID);
			    result= executeUpdate(statement);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =-123;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		return result;
	  }  
	
	public ArrayList<String>  getLotti(Connection conn,  String CURR_TIPO,ArrayList<String> CURR_STATO_LIST,String workProcessId ) throws Exception{
		String Operazione = "SELECT "+SCHEMA+".T_LOTTO ";
		PreparedStatement statement = null;
		ArrayList<String> customerSetIds = null;
		try {
			String sqlWorkProcessID  ="";
			if(workProcessId!=null && !workProcessId.isEmpty())sqlWorkProcessID= " AND \"WorkProcessID\"='"+workProcessId+"'";
			String query = "SELECT T_LOTTO.\"CustomerSetID\", T_LOTTO.ID_LOTTO, T_LOTTO.\"RagioneSocialeMittente\", T_LOTTO.\"versione-template\", T_LOTTO.\"WorkProcessID\", "
		    		 +" T_DELIVERY.\"DataEsito\", T_DELIVERY.\"DataNotifica\", T_DELIVERY.ID,  T_DELIVERY.IDENTIFICATIVO,"
		    		 +" T_E_TIPO.NOME AS TIPODELIVERY,"
		    		+"  T_E_STATO.NOME AS STATO"
		    		 +"  FROM T_LOTTO "
		    		 +" join T_R_CANALE ON T_R_CANALE.ID = T_LOTTO.ID_LOTTO"
		    		 +" join T_DELIVERY ON T_DELIVERY.ID = T_R_CANALE.ID_DELIVERY"
		    		 +" join T_E_TIPO ON T_DELIVERY.ID_TIPO = T_E_TIPO.ID"
		    		 +" join T_E_STATO ON T_DELIVERY.ID_STATO = T_E_STATO.STATO "
		    		 +" where T_DELIVERY.ID_STATO IN  "+getIN(CURR_STATO_LIST)+" AND T_DELIVERY.ID_TIPO = ?"
		    		 + sqlWorkProcessID;
			     statement = getPrepareStatment( query  ,conn);
			     statement.setString (1, CURR_TIPO);
			     ResultSet resultSet = executeQuery(statement);
			     customerSetIds = new ArrayList<String>();
			     while(resultSet.next()) {
			    	 customerSetIds.add(resultSet.getString("CustomerSetID"));
			     }
			     resultSet.close();
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	 
	    } finally {
            if (statement != null)
            	statement.close();
        }
		if(customerSetIds==null)throw new Exception("ERORRE " + Operazione );
		return customerSetIds;
	  }  
	
	
//	public void getEmps() throws SQLException{
//	    PreparedStatement statement = conn.prepareStatement("select "versione-template",CustomerSetID,WorkProcessID,RagioneSocialeMittente from T_LOTTO where ID_LOTTO = :ID_LOTTO");
//	    statement.setObject(?, "VALUE FOR ID_LOTTO");
//	    ResultSet resultSet = statement.executeQuery();
//	    while(resultSet.next()){
//	       System.out.println(resultSet.getObject(ID_LOTTO));
//	       System.out.println(resultSet.getObject("versione-template"));
//	       System.out.println(resultSet.getObject(CustomerSetID));
//	       System.out.println(resultSet.getObject(WorkProcessID));
//	       System.out.println(resultSet.getObject(RagioneSocialeMittente));
//	    }
//	  }   
	 
	
	

}
