package it.deck47.db.dao;

import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import hdi.lotto.OperazioniXMLLotto;
import it.deck47.db.DBOperationsHDI;

public class T_EMAIL_Dao extends UtilityDao implements IDao  {
	
//	public T_DOCUMENTO_Dao createValueObject() {
//        return new T_DOCUMENTO_Dao();
//    }
	
	public T_EMAIL_Dao(String schema) {
		initSchema(schema);
	} 
	
	public Long insert(Connection conn, String CUSTOMERENVELOPID,String TESTO,String DEST,String DESTCC,String ESITO,String ERRMESS, String DATAINVIO) throws Exception{
		String Operazione = "INSERT "+SCHEMA+".T_MAIL  ";
		PreparedStatement statement = null;
		Long result =(long) -10;
		try {
			     statement = getPrepareStatment("INSERT INTO "+SCHEMA+".T_MAIL " + 
			     		"(ID_DELIVERY, TESTO, DEST, DESTCC, ESITO, ERRMESS, DATAINVIO)" +
			     		"  VALUES  " + 
			     				"((select ID_DELIVERY from T_DOCUMENTO   " + 
					     		"			     join T_R_CANALE ON T_R_CANALE.ID = T_DOCUMENTO.ID_DOCUMENTO " + 
					     		"			     join T_DELIVERY ON T_DELIVERY.ID = T_R_CANALE.ID_DELIVERY " + 
					     		"			     where T_DOCUMENTO.\"CustomerEnvelopeID\" = ? " +
					     		"			     AND T_DELIVERY.ID_TIPO = 3" + 
					     		"			     AND T_DELIVERY.ID_STATO = 0 ),?,?,?,?,?,?)",conn);
			     
				 Timestamp datetime = Timestamp.valueOf(DATAINVIO);
				 
				 statement.setString (1, CUSTOMERENVELOPID);
				 statement.setString (2, TESTO);
			     statement.setString (3, DEST);
			     statement.setString (4, DESTCC);
			     statement.setString (5, ESITO);
			     statement.setString (6, ERRMESS);
			     statement.setTimestamp(7, datetime);
			    result= executeInsert(statement,false);
		}catch(Exception e) {
	    	log.severe("[ERRORE][SQL]"+Operazione+" : "+e.getMessage());
	    	result =(long) -123;
	    } finally {
            if (statement != null)
            	statement.close();
        }
		if(result<1)throw new Exception("Errore in "+Operazione+" CODE:"+result);
		return result;
	  }  

}
