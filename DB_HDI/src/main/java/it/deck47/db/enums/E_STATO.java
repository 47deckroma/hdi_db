package it.deck47.db.enums;

public enum E_STATO {CREATE(0),ERROR100ADOBE(100),ERROR1000DMS(1000),ERROR1500POSTEL(1500),
	QCS_NUOVO(11),QCS_INLAV(12),QCS_SPED(13),QCS_STOR(14),QCS_ERR(15),RLN_SPED(16);
	private  int id=-1;
	E_STATO(int id){
		this.id=id;
	}
	public int getId() {
		return id;
	}
	
	
	public static E_STATO getE_STATOfromQCS(String status) {
		E_STATO retESTATO  = QCS_ERR;
	 
		 switch (status) {
         case "1":  retESTATO = E_STATO.QCS_NUOVO;
         	break;
         case "2":  retESTATO = E_STATO.QCS_INLAV;
         	break;
         case "3":  retESTATO = E_STATO.QCS_SPED;
         	break;
         case "4":  retESTATO = E_STATO.QCS_STOR;
         break;
		 }
		 return retESTATO;
	}
	 
}
