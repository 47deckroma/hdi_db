package it.deck47.db.sigleton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.Timer;


public class SingletonPoolSafe {
	
	private final static SingletonPoolSafe singlePoolSafeIstance = new SingletonPoolSafe();
	
	private FUNCTION_STATUS addErrori=FUNCTION_STATUS.WAIT;

	public enum FUNCTION_STATUS{STOP,RUN,WAIT;	 
	} ;
	public enum FUNCTION_TYPE{ADD_ERRORI;	 
	} ;
	private SingletonPoolSafe(){
		initSingleton();
	}
	
	public static SingletonPoolSafe getSingletonPoolSafe(){
		return singlePoolSafeIstance;
	}
	/* Inizializzazione singleton thread safe
 	private volatile static SingletonVagliaStampa singleVagliaStampaIstance = null;
	
	public static  SingletonVagliaStampa getSingletonVagliaStampa(){
		if(singleVagliaStampaIstance==null){//Verifico l'istanza a null
		synchronized (SingletonVagliaStampa.class){
			if(singleVagliaStampaIstance==null){//Ricontrollo che sono il primo thread che accedo signleton
				singleVagliaStampaIstance = new SingletonVagliaStampa();
			}
		}}
		return singleVagliaStampaIstance;
	}
	 */

	private void initSingleton(){
	}

	public FUNCTION_STATUS getStatus(FUNCTION_TYPE istanceName) {
			return addErrori;
	}

	public int setStatus(FUNCTION_STATUS status, FUNCTION_TYPE istanceName,Logger log) {
			if(this.addErrori.equals(FUNCTION_STATUS.RUN) && status.equals(FUNCTION_STATUS.RUN) ){
				return -1;
			}
			
			if( status.equals(FUNCTION_STATUS.RUN) ){
				functionDisableAfter(60000);
			}
			
			if( status.equals(FUNCTION_STATUS.STOP) ){
				if(timer!=null && timer.isRunning()){
					timer.stop();
				}
			}
			
		if(log!=null)
			log.info("Set status "+istanceName+" to " +status);	
		this.addErrori = status;
		return 0;
	}
	
	private Timer timer ;
	private void functionDisableAfter(int timeout){
		try{
			 timer = new Timer(60000, new ActionListener() {
			  public void actionPerformed(ActionEvent arg0) {
				  setStatus(FUNCTION_STATUS.STOP,FUNCTION_TYPE.ADD_ERRORI,null);
			  }
			});
			timer.setRepeats(false); // Only execute once
			timer.start(); // Go go go!
		}catch(Exception e){
				System.out.println("Errore nel set stop function");
			}
	}

	//new Thread( new SignletonStampaRunnable(1)).start();
	public static class SignletonStampaRunnable implements Runnable{
		private int var;
		
		public SignletonStampaRunnable(int var){
			this.var = var;
		}
		
		public void run (){
			try {
				Thread.sleep(var);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			getSingletonPoolSafe().setStatus(FUNCTION_STATUS.WAIT,FUNCTION_TYPE.ADD_ERRORI,null);
		}
	}


	 
	

}
