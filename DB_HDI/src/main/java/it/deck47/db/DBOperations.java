package it.deck47.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.sql.DataSource;


public class DBOperations {

	private static final Logger log = Logger.getLogger(DBOperations.class.getName());

	private Properties prop =null;
	private boolean isPropValid() throws Exception {
		if (prop==null) {
			throw new Exception("Properties non correttamente inizializate per effettuare la connesione al server");
		}
		return true;
	}
	
	public static boolean closeConnection(Connection connection) {
		try {
		if(connection!=null) {
				if (!connection.isClosed()) {
					connection.close();
					connection=null;
				}else {
					log.warning("closeConnection non eseguita. Connessione precedentemente chiusa");
				}
		}else{
			log.warning("closeConnection non eseguita. Connessione NULL");
		}
		return true;
		} catch (SQLException e) {
			log.warning("closeConnection in Errore : " +e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean rollbackConnection(Connection connection) {
		try {
		if(connection!=null) {
				if (!connection.isClosed()) {
					connection.rollback();
 				}else {
					log.warning("rollbackConnection non eseguita. Connessione precedentemente chiusa");
				}
		}else{
			log.warning("rollbackConnection non eseguita. Connessione NULL");
		}
		return true;
		} catch (SQLException e) {
			log.warning("rollbackConnection in Errore : " +e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
	
	
	
	
	/**
	 * Metodo per creare la connessione al database, settare il livello della transazione a livello del singolo metodo
	 * @param serverAddress
	 * @param serverPort
	 * @param SID
	 * @param username
	 * @param password
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws Exception
	 */

	protected  Connection createConnection(String serverAddress, 
			String serverPort,
			String SID_SERVICE,
			String username,
			String password,
			String DataSource ) throws SQLException, ClassNotFoundException, Exception{


 		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection connection = null;
		
		int retry = 0;
		int limiteMassimo=6;
 		String sqlExceptionMessage="";
		do {
				try {
					if(DataSource==null || DataSource.isEmpty()) {
							String connectionString ="";
							//String connectionString = "jdbc:oracle:thin:@" + serverAddress + ":" + serverPort + ":" + SID;
							//String connectionString = "jdbc:oracle:thin:@//" + serverAddress + ":" + serverPort + "/" + SERVICE;
							if(serverAddress.toUpperCase().contains("(DESCRIPTION")){
								   connectionString ="jdbc:oracle:thin:@" + serverAddress;
							}else {
								  connectionString = "jdbc:oracle:thin:@" + serverAddress + ":" + serverPort + "" + SID_SERVICE;
							}
							connection = DriverManager.getConnection(connectionString,username,password);
					}else {
						InitialContext ctx = new InitialContext();
	 					DataSource ds  = ((DataSource) ctx.lookup("java:/"+DataSource));
	 					//UserTransaction userTrans =	(UserTransaction) ctx.lookup("java:comp/"+DataSource);
	 					//userTrans.begin();
						connection = ds.getConnection();
					}
					//if(connection!=null && connection.isValid(5))//Tolta da Alfredo per ottimizzare 
					if(connection!=null ) {
						retry=limiteMassimo+100;
						connection.setAutoCommit(false);
					}else {
						retry++;
						log.warning("Connessione "+serverAddress+" non riuscita; Riprovo ("+retry+")<("+limiteMassimo+")" );
					}
				}catch(Exception e ) {
					retry ++ ;
					log.warning("SQL Exception durante la connessione "+serverAddress+"; Riprovo ("+retry+")<("+limiteMassimo+")tra 10 secondi: "+ e.getMessage());
					 sqlExceptionMessage = e.getMessage();
					 if(connection!=null && !connection.isClosed()){
						 connection.close();
					 }
					 if(retry<limiteMassimo)
						 Thread.sleep(10000);
				}
		 }while(retry<limiteMassimo);
		
		if(retry==limiteMassimo){
			 throw new SQLException(sqlExceptionMessage);
		 }
		return connection;
	}

	protected Connection createConnection( Properties prop) throws SQLException, ClassNotFoundException, Exception{
		this.prop=prop;
		return createConnection(prop.getProperty("databaseURL"), prop.getProperty("databasePORT"),
				prop.getProperty("databaseSERVICE"), prop.getProperty("databaseUsername"),prop.getProperty("databasePassword"),
				prop.getProperty("databaseDataSource")
				) ;
	}
	

	
}
