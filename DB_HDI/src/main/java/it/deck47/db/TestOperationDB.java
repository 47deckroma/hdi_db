package it.deck47.db;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import it.deck47.db.DBOperationsHDI;
import it.deck47.db.enums.E_STATO;
import it.deck47.db.enums.E_TIPO;

/**
 *
 */
public class TestOperationDB 
{
	public static void main(String[] args) {
		ArrayList<String> listValue = new ArrayList<>();
		listValue.add("Prova");
		listValue.add("Prova");
		listValue.add("Provo");
		listValue.add("Prova");
		listValue.add("Errore");
		listValue.add("Prova");
		for (String valueString : listValue) {
			switch (valueString){
				case "Prova":{
					System.out.println("Prova");
				}break;
				case "Provo":{
					System.out.println("Provo");
				}break;
				default: {System.out.println("ERRORE");
					break;
				}
	             
			}
		}
	}
	
    	// http://localhost:8080/RESTfulExample/json/product/post
    	public static void main2(String[] args) throws Exception {
    		String xmlInput = TestOperationDB.readFile( "assets\\BTC006_20181107_000001.xml");
    		// String xmlInput = App.readFile( "assets\\XMLTest\\testMarco.xml");
    		 //String xmlBookmarks = App.readFile( "assets\\XMLTest\\bookmarkTest.xml");
    		 //String xmlInput = App.readFile( "assets\\XMLTest\\testQuietanzaErrore.xml");
    		 DBOperationsHDI db = new DBOperationsHDI();
    		 /** db.storeLotto : Salva i dati conenuti nell'xml nel DB*/
    		  db.storeLotto(xmlInput);
    		   System.out.println("SPLEEP");
    		   Thread.sleep(3000);
    		   System.out.println("RUN");
    		  //db.changeStatusLotto("AU.ISVAP_20181107_000001",null,null,E_STATO.ERROR100ADOBE);
    		// db.changeStatusDocumento("0060405653_0060_952774152_GENATR_BTC006_DMS025_SvilLocale",E_TIPO.POSTEL,null,E_STATO.ERROR100ADOBE);
    		 //EsitoResponseMPX response =  op.createMPX(xmlInput, "3", "wwww", "ZCode","Username", "Password", "ON", "tipoAmbiente", xmlBookmarks, MPXazioni.ARCHIVIAZIONE);
    		 //EsitoMPX response = op.sendToPostel(xmlInput,"10","pdfEncode64");
    		// System.out.println("\nESITO:" + EsitoMPX_Utility.toString(response) + response.getMPXString());
    	}
    	
    	public static String getTagValue(String xml, String tagName){
    	    return xml.split("<"+tagName+">")[1].split("</"+tagName+">")[0];
    	}
    	
    			
    		public static String readFile(String filePath) throws FileNotFoundException, IOException {
    			 String everything = null;
    			 try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
    				    StringBuilder sb = new StringBuilder("");
    				    String line = br.readLine();

    				    while (line != null) {
    				        sb.append(line);
    				        //sb.append(System.lineSeparator());
    				        line = br.readLine();
    				    }
    				    br.close();
    				     everything = sb.toString();
    				}catch(Exception e) {
    					e.printStackTrace();
    				}finally {
    					
    				}
    			 return everything;
    		 }
}
