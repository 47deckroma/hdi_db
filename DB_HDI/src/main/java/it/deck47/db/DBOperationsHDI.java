package it.deck47.db;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import hdi.lotto.Lotto;
import hdi.lotto.Lotto.Documento;
import hdi.lotto.OperazioniXMLLotto;
import it.adobe.deck47.ReadConfigFile;
import it.adobe.deck47.beans.E_WORKPROCESSID;
import it.adobe.deck47.beans.ItemDelivery;
import it.adobe.deck47.beans.ItemQuery;
import it.deck47.db.dao.T_DELIVERY_Dao;
import it.deck47.db.dao.T_DOCUMENTO_Dao;
import it.deck47.db.dao.T_EMAIL_Dao;
import it.deck47.db.dao.T_LOTTO_Dao;
import it.deck47.db.dao.T_MPX_Dao;
import it.deck47.db.dao.T_RELAZIONECANALE_Dao;
import it.deck47.db.dao.T_SMS_Dao;
import it.deck47.db.enums.E_STATO;
import it.deck47.db.enums.E_TIPO;

public class DBOperationsHDI extends DBOperations{

	private static final Logger log = Logger.getLogger(DBOperationsHDI.class.getName());
	
	
	
	public boolean insertList(ArrayList<ItemDelivery> itemsDelivery ) throws  Exception {
		if(itemsDelivery==null || itemsDelivery.isEmpty())return false;
		String operazione = this.getClass().getName()+" insertList :itemsDeliverySize="+itemsDelivery.size();
		boolean returnValue=false;
		Connection conn = null;
		try {
			 
			ReadConfigFile p = new ReadConfigFile();
		    conn = createConnection(p.getProperty(false));
		    String customerSerIdPrec = "";
		    
		    for (ItemDelivery item  : itemsDelivery) {
		    	switch (item.getType()){
		    	case "SMS"	:{
				    	T_SMS_Dao mpxDato = new T_SMS_Dao(p.getPropertyValue(false,"databaseSchema"));
				    	mpxDato.insert(conn, item.getCustomerEnvelopeID(), item.getText(), item.getDestinatario(), item.getErrorMessage(), item.getStatus(), item.getSendDate()) ;
				    	
//				    	if(!item.getCustomerSetID().equalsIgnoreCase(customerSerIdPrec)) {
//				    		customerSerIdPrec = item.getCustomerSetID();
//				    		changeStatusLotto(item.getCustomerSetID(), E_TIPO.SMS, E_STATO.CREATE, E_STATO.getE_STATOfromQCS(item.getStatus()));
//				    	}
		    	}break;
		    	case "EMAIL"	:{
			    	T_EMAIL_Dao mpxDato = new T_EMAIL_Dao(p.getPropertyValue(false,"databaseSchema"));
			    	mpxDato.insert(conn, item.getCustomerEnvelopeID(),item.getText(), item.getDestinatario(), item.getDestinatarioCC(), item.getStatus(), item.getErrorMessage(), item.getSendDate()) ;
			    	
//			    	if(!itemQuery.getCustomerSetID().equalsIgnoreCase(customerSerIdPrec)) {
//			    		customerSerIdPrec = itemQuery.getCustomerSetID();
//			    		changeStatusLotto(itemQuery.getCustomerSetID(), E_TIPO.POSTEL, E_STATO.QCS_SPED, E_STATO.RLN_SPED );
//			    	}
		    	}break;
		    }
		    }
		    	
		    	
		    
			log.info(operazione+" INSERT COMPLETATO");
			returnValue = true;
		}catch(Exception e) {
			log.severe("[ERRORE] (ROLLBACK)"+operazione+e.getMessage());
			rollbackConnection(conn);
			returnValue=false;
		}finally {
			closeConnection(conn);
		}
		return returnValue;
	}
	
	
	
	

	private static String getDateFromFiltroRicerca(Timestamp dataErrore) throws ParseException{
		SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date date = sdfSource.parse(dataErrore.toString());
		SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MM-yyyy");
		String s = sdfDestination.format(date);
		return s;

	}
	
	
	//DBOperationsHDI.getChunkList
	public static ArrayList<ArrayList<String>> getChunkList(ArrayList<String> largeList, int chunkSize){
		ArrayList<ArrayList<String>> returnlist = new ArrayList<ArrayList<String>>();
			if(largeList==null) return returnlist;
			if(largeList.size()<=chunkSize) 
				{returnlist.add(largeList);return returnlist;}
			int last = 0;
			for (int i=0; i< largeList.size(); i+= chunkSize) {
				 //last = i+ (chunkSize>=largeList.size()?  largeList.size():(i + chunkSize));
				 //System.out.println( largeList.size()+" ; "+(i + chunkSize));
				 last = (last+chunkSize)>largeList.size()?largeList.size():(last+chunkSize);  
				 //System.out.println(i+";"+last);
				List<String> list = largeList.subList(i,last );
				//System.out.println("Mezzo:"+list);
				returnlist.add(new ArrayList<String>( list));
			}
		
		return returnlist;
	}
	
	public static ArrayList<ArrayList<Long>> getChunkListLong(ArrayList<Long> largeList, int chunkSize){
		ArrayList<ArrayList<Long>> returnlist = new ArrayList<ArrayList<Long>>();
			if(largeList==null) return returnlist;
			if(largeList.size()<=chunkSize) 
				{returnlist.add(largeList);return returnlist;}
			int last = 0;
			for (int i=0; i< largeList.size(); i+= chunkSize) {
				 //last = i+ (chunkSize>=largeList.size()?  largeList.size():(i + chunkSize));
				 //System.out.println( largeList.size()+" ; "+(i + chunkSize));
				 last = (last+chunkSize)>largeList.size()?largeList.size():(last+chunkSize);  
				 //System.out.println(i+";"+last);
				List<Long> list = largeList.subList(i,last );
				//System.out.println("Mezzo:"+list);
				returnlist.add(new ArrayList<Long>( list));
			}
		
		return returnlist;
	}
	
	
	private static Object unmarsharl(Class jaxbObject ,String xmlinput ) throws Exception {
        JAXBContext jc = JAXBContext.newInstance(jaxbObject);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        StringReader sr = new StringReader(xmlinput);
        Object tests =  unmarshaller.unmarshal(sr);//cast (jaxbObject.getClass())
        //SystemOut
        //Marshaller marshaller = jc.createMarshaller();
        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        //marshaller.marshal(tests, System.out);
        return tests;
    }
	
	public boolean changeStatusLotto(String CustomerSetId,E_TIPO curr_tipo,E_STATO curr_stato,E_STATO next_stato) throws  Exception {
		String operazione = this.getClass().getName()+" changeStatusLotto :CustomerSetId="+CustomerSetId;
		boolean returnValue=false;
		Connection conn = null;
		try {
			int currTipo=-1;
			if(curr_tipo!=null) {
				currTipo = curr_tipo.getId();
			}
			ReadConfigFile p = new ReadConfigFile();
		    conn = createConnection(p.getProperty(false));
		    T_LOTTO_Dao lottoDao = new T_LOTTO_Dao(p.getPropertyValue(false,"databaseSchema"));
		    lottoDao.update(conn, CustomerSetId, currTipo , null ,next_stato.getId());
		    T_DOCUMENTO_Dao docDao = new T_DOCUMENTO_Dao(p.getPropertyValue(false,"databaseSchema"));
		    docDao.update(conn, CustomerSetId,  currTipo, null,next_stato.getId());
			log.info(operazione+" UPDATE COMPLETATO");
			returnValue = true;
		}catch(Exception e) {
			log.severe("[ERRORE]"+operazione+e.getMessage());
			rollbackConnection(conn);
			returnValue=false;
			log.severe("[ERRORE]"+operazione+" ROLLBACK");
		}finally {
			closeConnection(conn);
		}
		if(!returnValue)throw new Exception ("[ERRORE]"+operazione);
		return returnValue;
	}
	
	
	public boolean updateQCS(ArrayList<ItemQuery> itemsQuery ) throws  Exception {
		if(itemsQuery==null)return false;
		String operazione = this.getClass().getName()+" updateQCS :itemsQuerySize="+itemsQuery.size();
		boolean returnValue=false;
		Connection conn = null;
		try {
			 
			ReadConfigFile p = new ReadConfigFile();
		    conn = createConnection(p.getProperty(false));
		    String customerSerIdPrec = "";
		    for (ItemQuery itemQuery : itemsQuery) {
		    	switch (itemQuery.getType()){
		    	case "Set"	:{
				    	T_MPX_Dao mpxDato = new T_MPX_Dao(p.getPropertyValue(false,"databaseSchema"));
				    	mpxDato.updateMXPfromCustomerSetID(conn, itemQuery.getCustomerSetID(), itemQuery.getMPXSetID(), itemQuery.getUploadDate(), itemQuery.getNotifyDate(),"","");
				    	
				    	if(!itemQuery.getCustomerSetID().equalsIgnoreCase(customerSerIdPrec)) {
				    		customerSerIdPrec = itemQuery.getCustomerSetID();
				    		changeStatusLotto(itemQuery.getCustomerSetID(), E_TIPO.POSTEL, E_STATO.CREATE, E_STATO.getE_STATOfromQCS(itemQuery.getStatus()));
				    	}
		    	}break;
		    	case "Envelope"	:{
			    	T_MPX_Dao mpxDato = new T_MPX_Dao(p.getPropertyValue(false,"databaseSchema"));
			    	mpxDato.updateMXPfromCustomerEnvelopID(conn, itemQuery.getCustomerEnvelopeID(), itemQuery.getMPXSetID(), "", "",itemQuery.getCodRaccomandata(),itemQuery.getMailDate());
			    	
			    	if(!itemQuery.getCustomerSetID().equalsIgnoreCase(customerSerIdPrec)) {
			    		customerSerIdPrec = itemQuery.getCustomerSetID();
			    		changeStatusLotto(itemQuery.getCustomerSetID(), E_TIPO.POSTEL, E_STATO.QCS_SPED, E_STATO.RLN_SPED );
			    	}
		    	}break;
		    }
		    }
		    	
		    	
		    
			log.info(operazione+" UPDATE COMPLETATO");
			returnValue = true;
		}catch(Exception e) {
			log.severe("[ERRORE] (ROLLBACK)"+operazione+e.getMessage());
			rollbackConnection(conn);
			returnValue=false;
		}finally {
			closeConnection(conn);
		}
		return returnValue;
	}
	
	
	
	public  ArrayList<String> addStatus(ArrayList<String> valori, E_STATO stato) {
		if( valori==null ) {
			valori = new ArrayList<String>();
		}
		valori.add(String.valueOf(stato.getId()));
		return valori;
	}
	
	
	
	public java.util.HashMap<String,String>  getLotti_QCS( E_TIPO CURR_TIPO,  ArrayList<String>  CURR_STATOs  ) throws  Exception {
		 
		String operazione = this.getClass().getName()+" getLotti_QCS : Tipo:"+CURR_TIPO.name()+" Stato:"+CURR_STATOs;
		Connection conn = null;
		
		java.util.HashMap<String,String> valori = null;
		try {
			ReadConfigFile p = new ReadConfigFile();
		    conn = createConnection(p.getProperty(false));
		    T_LOTTO_Dao dao = new T_LOTTO_Dao(p.getPropertyValue(false,"databaseSchema"));
		    ArrayList<String>    customerSetIds = dao.getLotti (conn,String.valueOf(CURR_TIPO.getId()),  CURR_STATOs,null );
		     
		     for ( String custSetId : customerSetIds) {
		    	valori =  addQuery(valori, "CustomerSetID", custSetId);
		     }
		}catch(Exception e) {
			log.severe("[ERRORE]  "+operazione+e.getMessage());
		}finally {
			closeConnection(conn);
		}
		return valori;
	}
	
	public static void main2(String[] args) throws Exception {
		DBOperationsHDI db = new DBOperationsHDI();
		ArrayList<String> valori = new ArrayList<>();
		db.addStatus(valori, E_STATO.CREATE);
		db.addStatus(valori, E_STATO.QCS_INLAV);
		java.util.HashMap<String,String>  hm = db.getLotti_RLN(E_TIPO.POSTEL, valori);
		System.out.println(hm);
	}
	
	public static void main(String[] args) throws Exception {
		DBOperationsHDI db = new DBOperationsHDI();
		 ArrayList<ItemDelivery> itemsDelivery = new ArrayList<>();
		 ItemDelivery e = new ItemDelivery();
		 
		itemsDelivery.add(e);
		db.insertList(itemsDelivery);
		 
	}
	
	public java.util.HashMap<String,String>  getLotti_RLN( E_TIPO CURR_TIPO,  ArrayList<String>  CURR_STATOs  ) throws  Exception {
		 
		String operazione = this.getClass().getName()+" getLotti_QCS : Tipo:"+CURR_TIPO.name()+" Stato:"+CURR_STATOs;
		Connection conn = null;
		
		java.util.HashMap<String,String> valori = null;
		try {
			ReadConfigFile p = new ReadConfigFile();
		    conn = createConnection(p.getProperty(false));
		    T_LOTTO_Dao dao = new T_LOTTO_Dao(p.getPropertyValue(false,"databaseSchema"));
		    ArrayList<String>    customerSetIds = dao.getLotti (conn,String.valueOf(CURR_TIPO.getId()),  CURR_STATOs ,E_WORKPROCESSID.RAC.getId() );
		     
		     for ( String custSetId : customerSetIds) {
		    	valori =  addQuery(valori, "CustomerSetID", custSetId);
		     }
		}catch(Exception e) {
			log.severe("[ERRORE]  "+operazione+e.getMessage());
		}finally {
			closeConnection(conn);
		}
		return valori;
	}
	
	private static java.util.HashMap<String,String> addQuery(java.util.HashMap<String,String> valori,String keyName ,String value) {
		if( valori==null ) valori = new java.util.HashMap<String,String>();
		boolean insert=false;
		while(insert==false) {
			if(valori.containsKey(keyName)) {
				//System.out.println(keyName);
				keyName = keyName+ new Random().nextInt(100);
			}else {
				valori.put(keyName, value);
				insert = true;
			}
		}
		return valori;
	}
	
	
	public boolean changeStatusDocumento(String CustomerEnvelopeId,E_TIPO curr_tipo,E_STATO curr_stato,E_STATO next_stato) throws  Exception {
		String operazione = this.getClass().getName()+" changeStatusDocumento :CustomerEnvelopeId="+CustomerEnvelopeId;
		boolean returnValue=false;
		Connection conn = null;
		try {
			int currTipo=-1;
			if(curr_tipo!=null) {
				currTipo = curr_tipo.getId();
			}
			ReadConfigFile p = new ReadConfigFile();
		    conn = createConnection(p.getProperty(false));
		     
		    T_DOCUMENTO_Dao docDao = new T_DOCUMENTO_Dao(p.getPropertyValue(false,"databaseSchema"));
		    docDao.updateByCustomerEnvelop (conn, CustomerEnvelopeId,  currTipo, null,next_stato.getId());
			log.info(operazione+" UPDATE COMPLETATO");
			returnValue = true;
		}catch(Exception e) {
			log.severe("[ERRORE]"+operazione+e.getMessage());
			rollbackConnection(conn);
			returnValue=false;
			log.severe("[ERRORE]"+operazione+" ROLLBACK");
		}finally {
			closeConnection(conn);
		}
		return returnValue;
	}
	 /** db.storeLotto : Salva i dati contenuti nell'xml nel DB
	  * @author Alfredo Palma
	  * @exception  SQLException Errore SQL
	  * @exception  Exception Errore generico
	  * @param xmlInput : Xml della richiesta lotto
	  * @return boolean true : tutto a buon fine
	  * */
	public boolean storeLotto( String xmlInput) throws SQLException, Exception {
		String operazione = this.getClass().getName()+" storeLotto :";
		boolean  returnValue=false;
		Lotto f = (Lotto) unmarsharl(Lotto.class,xmlInput );
		ReadConfigFile p = new ReadConfigFile();
		Connection conn = null;
		T_LOTTO_Dao lottoDao = new T_LOTTO_Dao(p.getPropertyValue(false,"databaseSchema"));
		try {
		    conn = createConnection(p.getProperty(false));
			//Insert Lotto
			Long idLotto = lottoDao.insert(conn, f);
			log.info("ID_LOTTO:"+idLotto);
			if(idLotto==null)throw new Exception("ID lotto non ricevuto; Problemi durante lo store del lotto");
			
			//Insert delivery,status e relazione lotto
			for (E_TIPO tipologieDelivery :  E_TIPO.values()) {
				
				if(f.getServizio().getCodice()!=null && f.getServizio().getCodice().equalsIgnoreCase(tipologieDelivery.name())) {
					//POSTEL
					Long status = storeStatus(conn,idLotto,tipologieDelivery.getId(),E_STATO.CREATE.getId(),p.getPropertyValue(false,"databaseSchema"));
					if(status==null || status<0)throw new Exception("ID Status non ricevuto; Problemi status Lotto tipo "+tipologieDelivery);
				}
				if(f.getTipologiaDms()!=null && f.getTipologiaDms().equalsIgnoreCase(tipologieDelivery.name())) {
					//DMS
					Long status = storeStatus(conn,idLotto,tipologieDelivery.getId(),E_STATO.CREATE.getId(),p.getPropertyValue(false,"databaseSchema"));
					if(status==null || status<0)throw new Exception("ID Status non ricevuto; Problemi status Lotto tipo "+tipologieDelivery);
				}
				
			}
			
			//Insert Documenti
			T_DOCUMENTO_Dao docDao = new T_DOCUMENTO_Dao(p.getPropertyValue(false,"databaseSchema"));
			 HashMap< String,Long> documenti = docDao.insertMap(conn, f.getDocumento(),idLotto,p.getPropertyValue(false,"TipoAmbiente"));
			if(documenti==null || documenti.size()<1)throw new Exception("ID documenti non ricevuto; Problemi durante lo store del documenti");
 			
			//Insert delivery,status e relazione documenti
			for (Documento doc : f.getDocumento()) {
				String customerEnvelopId = OperazioniXMLLotto.getCustomerEnvelopeID(doc, p.getPropertyValue(false,"TipoAmbiente"));
				for (E_TIPO tipologieDelivery :  E_TIPO.values()) {
					//POSTEL
					if(doc.getTestata().getServizio().getCodice()!=null && doc.getTestata().getServizio().getCodice().equalsIgnoreCase(tipologieDelivery.name())) {
						Long statusDocs = storeStatus(conn,documenti.get(customerEnvelopId),tipologieDelivery.getId(),E_STATO.CREATE.getId(),p.getPropertyValue(false,"databaseSchema"));
		 				if(statusDocs==null || statusDocs<0)throw new Exception("ID Status non ricevuto; Problemi durante lo store status documento tipo:"+tipologieDelivery);
					}
					//DMS
					if(doc.getTestata().getTipologiaDms()!=null && doc.getTestata().getTipologiaDms().equalsIgnoreCase(tipologieDelivery.name())) {
						Long statusDocs = storeStatus(conn,documenti.get(customerEnvelopId),tipologieDelivery.getId(),E_STATO.CREATE.getId(),p.getPropertyValue(false,"databaseSchema"));
		 				if(statusDocs==null || statusDocs<0)throw new Exception("ID Status non ricevuto; Problemi durante lo store status documento tipo:"+tipologieDelivery);
					}
					//SMS Per ogni documento se è un sms inseriamo una delivery di tipo SMS 
					boolean sms = false;
					try {
						doc.getTestata().getDestinatari().getSoggetto().get(0).getModalitaInvio().getSms();
						sms=true;
					}catch(Exception e) {log.severe("[ERRORE]"+operazione+e.getMessage());}
					if(sms) {
						Long statusDocs = storeStatus(conn,documenti.get(customerEnvelopId),E_TIPO.SMS.getId(),E_STATO.CREATE.getId(),p.getPropertyValue(false,"databaseSchema"));
		 				if(statusDocs==null || statusDocs<0)throw new Exception("ID Status non ricevuto; Problemi durante lo store status documento tipo SMS, Envelop:" + customerEnvelopId);
					}
					//SMS Per ogni documento se è un sms inseriamo una delivery di tipo SMS 
					boolean mail = false;
					try {
						doc.getTestata().getDestinatari().getSoggetto().get(0).getModalitaInvio().getEmail();
						mail=true;
					}catch(Exception e) {log.severe("[ERRORE]"+operazione+e.getMessage());}
					if(mail) {
						Long statusDocs = storeStatus(conn,documenti.get(customerEnvelopId),E_TIPO.EMAIL.getId(),E_STATO.CREATE.getId(),p.getPropertyValue(false,"databaseSchema"));
		 				if(statusDocs==null || statusDocs<0)throw new Exception("ID Status non ricevuto; Problemi durante lo store status documento tipo MAIL, Envelop:" + customerEnvelopId);
					}
				}
			}
			
			log.info("STORE COMPLETATO");
			returnValue = true;
			//conn.commit();
			
		}catch(Exception e) {
			log.severe("[ERRORE]"+operazione+e.getMessage());
			rollbackConnection(conn);
			returnValue=false;
			log.severe("[ERRORE]"+operazione+" ROLLBACK");
		}finally {
			closeConnection(conn);
		}
		return returnValue;
	} 
	
	private Long storeStatus( Connection conn , Long idLotto,int tipo, int stato,String schema) throws SQLException, Exception {
		String operazione = this.getClass().getName()+" STORE  STATUS :";
		Long returnCode =-1L;
		try {
			T_DELIVERY_Dao delivery = new T_DELIVERY_Dao(schema);
			Long iddelivery = delivery.insert(conn, tipo, stato);
			if(iddelivery==null)throw new Exception("ID DELIVERY non ricevuto; Problemi durante lo store del lotto");
			T_RELAZIONECANALE_Dao relazione = new T_RELAZIONECANALE_Dao(schema);
			//Insert T_MPX
			T_MPX_Dao mpxDAO = new T_MPX_Dao(schema);
			Long mpxId = mpxDAO.insert(conn, iddelivery);
			if(mpxId!=1)throw new Exception("T_MPX aggiornati/inseriti difforme da quanto atteso");
			Long idRelazione  =relazione.insert(conn, idLotto, iddelivery);
			if(idRelazione==null)throw new Exception("ID RELAZIONE non ricevuto; Problemi durante lo store del lotto");			
			returnCode = 0L;
		}catch(Exception e) {
			log.severe("[ERRORE]"+operazione+e.getMessage());
			//conn.rollback();
			returnCode= -1L;
		}finally {
			//closeConnection(conn);
		}
		return returnCode;
	} 
	
	private Long storeStatusList( Connection conn , ArrayList<Long> idDoc,int tipo, int stato,String schema) throws SQLException, Exception {
		String operazione = this.getClass().getName()+" STORE  STATUS :";
		Long returnCode =-1L;
		try {
			T_DELIVERY_Dao delivery = new T_DELIVERY_Dao(schema);
			ArrayList<java.lang.Long> iddelivery = delivery.insert(conn, tipo, stato,idDoc.size());
			if(iddelivery==null|| iddelivery.size()==0)throw new Exception("ID DELIVERY non ricevuto; Problemi durante lo store del lotto");
			T_RELAZIONECANALE_Dao relazione = new T_RELAZIONECANALE_Dao(schema);
			int relazioniInserite=relazione.insertList(conn, idDoc, iddelivery);
			if(relazioniInserite<1)throw new Exception("ID RELAZIONE non ricevuto; Problemi durante lo store del lotto");			
			returnCode = 0L;
		}catch(Exception e) {
			log.severe("[ERRORE]"+operazione+e.getMessage());
			//conn.rollback();
			returnCode= -1L;
		}finally {
			//closeConnection(conn);
		}
		return returnCode;
	} 
	
	



}
